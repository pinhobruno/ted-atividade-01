package com.ucsal.ted01.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucsal.ted01.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

}
