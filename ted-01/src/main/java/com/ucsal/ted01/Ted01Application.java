package com.ucsal.ted01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ted01Application {

	public static void main(String[] args) {
		SpringApplication.run(Ted01Application.class, args);
	}

}
