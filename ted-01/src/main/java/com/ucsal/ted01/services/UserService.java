package com.ucsal.ted01.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ucsal.ted01.dto.UserDTO;
import com.ucsal.ted01.entities.User;
import com.ucsal.ted01.repositories.UserRepository;
import com.ucsal.ted01.services.exceptions.DatabaseException;
import com.ucsal.ted01.services.exceptions.ResourceNotFoundException;

@Service
public class UserService {

	@Autowired
	private UserRepository repository;
	
	@Transactional(readOnly = true)
	public Page<UserDTO> pagedSearch(Pageable pageable){
		Page<User> list = repository.findAll(pageable);
		return list.map(UserDTO::new);
	}

	@Transactional(readOnly = true)
	public List<UserDTO> findAll(){
		return repository.findAll().stream().map(UserDTO::new).collect(Collectors.toList());
	}
	@Transactional
	public UserDTO save(UserDTO dto) {
		User entity = toEntity(dto);
		repository.save(entity);
		return new UserDTO(entity);
	}

	@Transactional
	public UserDTO update(Long id, UserDTO dto) {
		try {
			User entity = toUpdate(id, dto);
			repository.save(entity);
			return new UserDTO(entity);
		} catch (EntityNotFoundException e) {
			throw new ResourceNotFoundException("ID not found : " + id);
		}
	}

	@Transactional(readOnly = true)
	public UserDTO findById(Long id) {
		return repository.findById(id).map(UserDTO::new)
				.orElseThrow(() -> new ResourceNotFoundException("Entity not found"));
	}

	public void delete(Long id) {
		try {
			repository.deleteById(id);
		} catch (DataIntegrityViolationException e) {
			throw new DatabaseException("ID not found : " + id);
		} catch (EmptyResultDataAccessException e) {
			throw new ResourceNotFoundException("ID not found : " + id);
		}
	}

	// Auxiliary methods

	public User toEntity(UserDTO dto) {
		User entity = new User();
		entity.setName(dto.getName());
		entity.setEmail(dto.getEmail());
		return entity;
	}

	public User toUpdate(Long id, UserDTO dto) {
		User entity = repository.getById(id);
		entity.setName(dto.getName());
		entity.setEmail(dto.getEmail());
		return entity;
	}
}
